<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:template match="/Pedidos">
	<html>
	<meta charset="UTF-8"/>
		<head> 
		 <style>
table {
  border-collapse: collapse;
  width: 100%;
  border: 3px solid black; /* Estilo da borda */
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

h1 {
  text-align: center;
}
  </style>
		</head>
		<body>
			<h1 style="text-align: center;">Pedidos</h1>
			<table border="1">
			<!-- grid header -->
			<tr bgcolor="lightgreen">
				<td>Número do Pedido</td>
				<td>Data do Pedido</td>
				<td>Custo</td>
				<td>Representante</td>
				<td>Fabricante</td>
				<td>Produto</td>
				<td>Quantidade</td>
       			<td>Montante</td>
				<td>Comissão</td>
				<td>Valor Total Pedido</td>
				</tr>
				<xsl:apply-templates select="Pedido">
				<xsl:sort select="title" />
				</xsl:apply-templates>
			</table>
		</body>
	</html>
</xsl:template>
<xsl:template match="Pedido">
	<!-- grid value fields -->
	<tr>
		<td><xsl:value-of select="order_num"/></td>
		<td><xsl:value-of select="order_date"/></td>
		<td><xsl:value-of select="cust"/></td>
		<td><xsl:value-of select="rep"/></td>
		<td><xsl:value-of select="mfr"/></td>
		<td><xsl:value-of select="product"/></td>
		<td><xsl:value-of select="qty"/></td>
		<td><xsl:value-of select="amount"/></td>
		<td><xsl:value-of select="comissao"/></td>
		<td><xsl:value-of select="totalcomissAmount"/></td>
	</tr>
</xsl:template>
</xsl:stylesheet>